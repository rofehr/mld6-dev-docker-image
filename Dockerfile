FROM debian:buster

RUN apt-get update \
 && apt-get install -y \
		gawk wget git-core diffstat unzip texinfo gcc-multilib build-essential chrpath socat cpio python python3 python3-pip python3-pexpect xz-utils debianutils iputils-ping libsdl1.2-dev xterm locales locales-all default-jre acl \
		openssh-server nginx cron sudo \
		vim mc htop lz4 zstd zstd\
 && apt clean

RUN pip3 install kas

# nginx für Zugriff auf die deb Pakete
#RUN echo "server {\n listen 8000;\n server_name _;\n location ~ ^/$ {\n  autoindex on;\n  alias /home/\$user;\n }\n location ~ ^/#firmware/(?<file>.*)$ {\n  absolute_redirect off;\n  autoindex on;\n  alias /home/firmware/\$file;\n }\n location ~ ^/(?<user>[^/]*)/(?#<file>.*)$ {\n  absolute_redirect off;\n  autoindex on;\n  alias /home/\$user/mld-dev/deploy/\$file;\n }\n location ~ ^/favicon.ico$ #{\n  alias /var/www/html/favicon.ico;\n }\n}" > /etc/nginx/sites-available/apt \
# && ln -s /etc/nginx/sites-available/apt /etc/nginx/sites-enabled/apt

# Benutzer erzeugen
RUN useradd -s /bin/bash -g users -G sudo -m nightbuild \
 && mkdir -p /home/share \
 && setfacl -R -d -m g::rwx /home/share/ \
 && mkdir -p /home.template \
 && mv /home/* /home.template/

# Nightbuild cron job
#RUN echo "0 4 * * * /home/nightbuild/mld-dev/buildall.sh > /home/nightbuild/mld-dev/buildall.log" > /var/spool/cron/crontabs/nightbuild \
# && chown nightbuild:crontab /var/spool/cron/crontabs/nightbuild \
# && chmod 600 /var/spool/cron/crontabs/nightbuild


RUN echo "%sudo ALL=(ALL:ALL) NOPASSWD:ALL" >> /etc/sudoers \
 && echo 'de_DE.UTF-8 UTF-8' >> /etc/locale.gen && locale-gen \
 && echo "umask 0002" >> /etc/bash.bashrc
# && echo '$if mode=emacs'\n'"\e[5~": history-search-backward'\n'"\e[6~": history-search-forward'\n'$endif' >> /etc/inputrc

RUN sed -i '/session    required     pam_loginuid.so/c\#session    required   pam_loginuid.so' /etc/pam.d/cron \
 && echo 'if [ ! "$(ls /home)" ]; then cp -a /home.template/* /home; fi' > /init.sh \
 && echo '/etc/init.d/cron start\n/etc/init.d/ssh start\n/etc/init.d/nginx start\nsleep infinity' >> /init.sh \
 && chmod a+x /init.sh

EXPOSE 22
EXPOSE 8000

CMD /init.sh
